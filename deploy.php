<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'application');

// Project repository
set('repository', 'git@bitbucket.org:siduko/wit-demo.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', [
    'logs'
]);

// Writable dirs by web server 
set('writable_dirs', [
    'logs'
]);


// Hosts

host('aliostek-2')
    ->user('wit-demo')
    ->set('deploy_path', '~/{{application}}');    
    

// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
