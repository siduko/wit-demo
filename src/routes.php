<?php

use Slim\Http\Request;
use Slim\Http\Response;
use GuzzleHttp\Client;

// Routes


$app->post('/incoming', function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    $this->logger->info('Body Data: ' . json_encode($data));

    // Check challenge
    if (isset($data['challenge'])) {
        return $response
            ->withJson([
                'challenge' => $data['challenge']
            ]);
    }

    $token = $data['token'];
    $event = $data['event'];
    $authedUsers = $data['authed_users'];

    $eventType = $event['type'];
    $channel = $event['channel'];
    $user = $event['user'];

    // is bot
    if ($user == 'UED8022AZ') {
        return;
    }

    $slackClient = new Client([
        'base_uri' => 'https://slack.com/api',
        'headers' => [
            'Authorization' => 'Bearer xoxb-6827619606-489272070373-OH2F4bdV2jz2KFNZPXKKe3mc',
            'Content-Type' => 'application/json; charset=utf-8'
        ]
    ]);

    switch ($eventType) {
        case 'message':
            $text = $event['text'];
            $slackResponse = $slackClient->post('/chat.postMessage', [
                'json' => [
                    'token' => $token,
                    'text' => 'You said: ' . $text,
                    'channel' => $channel
                ]
            ]);
            $this->logger->info('Slack response: ' . $slackResponse->getBody());

            $slackResponse = json_encode($slackResponse->getBody());
            break;
        default:
            $slackClient->post('/chat.postMessage', [
                'json' => [
                    'token' => $token,
                    'text' => 'I don\'t understand what you said',
                    'channel' => $channel
                ]
            ]);


            break;
    }
});

//$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
//    // Sample log message
//    $this->logger->info("Slim-Skeleton '/' route");
//
//    // Render index view
//    return $this->renderer->render($response, 'index.phtml', $args);
//});
